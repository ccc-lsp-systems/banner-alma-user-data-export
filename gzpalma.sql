SET ECHO OFF;
SET VERIFY OFF;
SET FEEDBACK OFF;
SET DOC OFF;
/*---------------------------------------------------------------------------------
gzpalma.sql                              v8.03                             May 2015
                                Colorado School of Mines

DESCRIPTION:  Read previously loaded Human Resources (HR) and Student (STU) Library 
              temp tables to load the LOCAL.gzbalma processing table. HR records 
              are inserted first and student records thereafter, as long as the
              to-be-loaded student record has not already been inserted into LO-
              CAL.gzbalma as an employee. LOCAL.gzbalma is then read and the com-
              bined STU and HR records are then written to the LOCAL.gzbalma_load 
              table, along with the .xml-related header tags and info, detail tags 
              and info, and footer tag and info.  At end of process, the LOCAL.
              gzbalma_load table is spooled into a csmpatronload.xml file which, 
              is zipped and loaded to a designated CCIT target server, where it
              will be picked up by the vendor (ALMA).  This program runs through 
              UC4.  It may also be run on an as-needed, ad-hoc basis from Banner 
              jobsub. Note, however, that if you do the latter, you will need to 
              manually zip the csmpatronload.xml file (from the Linux command
              line) and have CCIT Server Team personnel load it into the correct 
              target server location, since developers will not have write access 
              to said location.

              This program works with temporary tables only. No dml occurs on any
              Banner Production tables.

REQUESTED BY: Laura Guy, Arthur Lakes Library, (303)384-2355

RETURN CODE:  0                       - SQL*Plus terminated without error.
              sql.sqlcode             - SQL*Plus terminated with an error.

SYNTAX:       This  program may be run through Banner job submission or the UC4
              scheduler. In order for the csmpatronload.xml file to be written
              to the target CCIT server, the job must be submitted through the
              UC4 scheduler.

TABLES:       bansecr.guraobj         - read
              LOCAL.pzbalma           - read
              LOCAL.szbalma           - read
              gobtpac                 - read
              LOCAL.gzbalma           - delete, read, insert and update
              LOCAL.gzbalma_load      - delete, insert and read

FILES:        "gzpalma_oneupno.lis"   - default referral msg
              "gzpalma_oneupno.lis2"  - records skipped in .xml write due to a
                                        goremal_email value > 49 characters.
              "gzpalma_oneupno.log"   - job parms and eoj msgs
              "csmpatronload.xml"     - detail information extract which, when
                                        zipped, will be retrieved by Alma from
                                        a CCIT-set-up server location.
              "csmpatronload.zip"     - Linux-command-line-zipped versiion of
                                        csmpatronload.xml detail information 
                                        extract described above.
                                                                                
REPORT FORMAT:  n/a

REVISION HISTORY:

Date:       Author:   Description:                                      Proj ID:
---------   -------   -----------------------------------------------   -----------
04/01/15    sco       Alma Joint Libr Patron Load program coded for     GZPALMA
                      Alma system use.  Reference to p_csm_send_mail_
                      via_utl_smtp changed to gzpnoti, to comply with 
                      Ellucian naming conventions. Also assigned be-
                      ginning version number, month and year.
04/23/15    sco       Program reference changed ONLY to accommodate     GZPALMA-2
                      removal of load_user_group column and addition
                      of load_end_user_identifiers column in LOCAL.
                      gzbalma_load. .xml works just fine, problem
                      was ES troubleshooting in that fields were all
                      one off, since load_user_group column info is,
                      by design, concatenated and contained in the
                      already existing load_user_group_desc column.
05/14/15    sco       Adjust program to flag records with goremal_      GZPALMA-3
                      email length > 49, write them to .lis2 file
                      and do not include them when selecting rec-
                      ords to write in .xml format.  Newly allowed
                      Mines larger e-mail name format was causing
                      Alma load to fail, since linesize of .xml 
                      file was exceeded for long names in LOAD_
                      EMAIL_ADDRESS column.  Also increased line-
                      size from 80 to 90 to accommodate the cur-
                      ent longest allowable e-mail name.  Version
                      (and month) changed from 8.02 (April) to 8.03
                      (May) to reflect these modifications.

AUDIT TRAIL END
---------------------------------------------------------------------------------*/
/*----------------------------------------------------*/
/* CLEAR ALL RECORDS FROM EXTERNALLY CREATED TEMPOR-  */
/* ARY TABLE.                                         */
/*----------------------------------------------------*/
DELETE FROM LOCAL.gzbalma;
DELETE FROM LOCAL.gzbalma_load;
COMMIT;

WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK;
WHENEVER OSERROR  EXIT SQL.SQLCODE ROLLBACK;
SET SHOWMODE OFF;
SET TIME OFF;
SET TIMING OFF;
SET TERMOUT ON;
SET SERVEROUTPUT ON SIZE 1000000;


        -------------------------- DECLARE BIND VARIABLES -----------------------

        VARIABLE        bOutputFile     VARCHAR2(70);
        VARIABLE        bOutputFile2    VARCHAR2(70);
        VARIABLE        bOutputFile3    VARCHAR2(70);
        VARIABLE        bflag           VARCHAR2(1);
        VARIABLE        bdefault_msg    VARCHAR2(50);
                                                      
        --- this variable is used to hold an exception-generated error  ---
        --- message, which will print in the .log and .lis files. note  ---
        --- that different errors will use the same variable.           ---

        VARIABLE        berror_msg      VARCHAR2(200);


DECLARE

        --------------------- JOBSUB VARIABLES --------------------------

        JobName         VARCHAR2(30)    := UPPER('&1');
        OneUpNo         NUMBER(8)       := TO_NUMBER('&2');
        OutputFile      VARCHAR2(70)    := '&3';
        OutputFile2     VARCHAR2(70)    := 'csmpatronload.xml';
        OutputFile3     VARCHAR2(70)    := LOWER('&1')||'_'||LOWER('&2')||'.lis2';
        v_today         VARCHAR2(25)    := NULL;
        v_user          VARCHAR2(15)    := NULL;
        v_curr_vers     VARCHAR2(10)    := '8.03';


        ----------------- PROGRAM-SPECIFIC VARIABLES, ----------------
        ------------------- CURSORS, FUNCTIONS AND -------------------
        ------------------------ EXCEPTIONS --------------------------

        p_auto_sched                    VARCHAR2(1)   := NULL;
        
        v_message_text                  VARCHAR2(80)  := NULL;
        
        default_pref_language_desc      CONSTANT VARCHAR2(7)  := 'English';
        default_pref_language           CONSTANT VARCHAR2(2)  := 'en';
        default_user_status_desc        CONSTANT VARCHAR2(6)  := 'Active';
        default_user_status             CONSTANT VARCHAR2(6)  := 'ACTIVE';
        default_user_account_type_desc  CONSTANT VARCHAR2(8)  := 'External'; 
        default_user_account_type       CONSTANT VARCHAR2(8)  := 'EXTERNAL';
        default_line1                   CONSTANT VARCHAR2(17) := '1500 Illinois St.';
        default_city                    CONSTANT VARCHAR2(6)  := 'Golden';    
        default_state_province          CONSTANT VARCHAR2(2)  := 'CO';  
        default_postal_code             CONSTANT VARCHAR2(10) := '80401-0000';
        default_country                 CONSTANT VARCHAR2(2)  := 'US';        
        default_address_type_desc       CONSTANT VARCHAR2(4)  := 'Work'; 
        default_address_type            CONSTANT VARCHAR2(4)  := 'work';         
        default_email_type_desc         CONSTANT VARCHAR2(4)  := 'Work'; 
        default_email_type              CONSTANT VARCHAR2(4)  := 'work';                              
        default_phone_type_desc         CONSTANT VARCHAR2(6)  := 'Office'; 
        default_phone_type              CONSTANT VARCHAR2(6)  := 'office';  
                

        /*---------------------------------------------------------*/
        /* check to see if the version of the program being run is */
        /* the same as what Banner security expects it to be. if   */
        /* not, write msg to all output files and end the program. */
        /*---------------------------------------------------------*/
        CURSOR curr_vers_cur
        IS
           SELECT guraobj_current_version
             FROM bansecr.guraobj
            WHERE guraobj_object = JobName;


        /*-------------------------------------------------------*/
        /* read hr patron records from hr temp table and load    */
        /* them into LOCAL.gzbalma for an eventual combined up-  */
        /* load to ALMA below, as long as a blasterid exists.    */
        /*-------------------------------------------------------*/
        CURSOR get_hr_patrons_cur
        IS
           SELECT DISTINCT t_pzpalma_pidm,
                  t_pzpalma_id,
                  t_pzpalma_blasterid,
                  t_pzpalma_last_name,
                  t_pzpalma_first_name,
                  SUBSTR(t_pzpalma_mi,1,1) a_mi,
                  t_pzpalma_bus_email,
                  t_pzpalma_bus_phone,
                  'Faculty/Staff' a_user_group_desc,
                  t_pzpalma_user_group,
                  DATA_SOURCE,                  
                  t_calc_expiry_date
             FROM LOCAL.pzbalma
            WHERE t_pzpalma_blasterid IS NOT NULL
         ORDER BY t_pzpalma_last_name, t_pzpalma_first_name, t_pzpalma_id; 


        /*-------------------------------------------------------*/
        /* read student patron records from hr temp table and    */
        /* load them into LOCAL.gzbalma table for an eventual,   */
        /* combined upload to ALMA below, as long as long as     */
        /* the same CWID was not already loaded into LOCAL.      */
        /* gzbalma_from LOCAL.pzbalma.                           */
        /*-------------------------------------------------------*/ 
        CURSOR get_stu_patrons_cur
        IS
           SELECT DISTINCT szbalma_pidm,
                  szbalma_id,
                  szbalma_last_name,
                  szbalma_first_name,
                  SUBSTR(szbalma_mi,1,1) b_mi,
                  szbalma_email,
                  '0000000000' b_fone,
                  szbalma_levl_code,
                  szbalma_dept_code,
                  szbalma_blasterid,
                  DECODE(szbalma_user_group_code,'UStu','Undergrad Student','Grad Student') b_user_group_desc,
                  szbalma_user_group_code,
                  szbalma_expiry_date
             FROM LOCAL.szbalma
            WHERE szbalma_blasterid IS NOT NULL
              AND NOT EXISTS (select 'x'
                                from LOCAL.gzbalma
                               where t_gzbalma_cwid = szbalma_id)
         ORDER BY szbalma_last_name, szbalma_first_name, szbalma_id; 


        /*-------------------------------------------------------*/ 
        /* add shibboleth info to .xml output file. info appears */
        /* in UID user_identifier section.                       */ 
        /*-------------------------------------------------------*/ 
        CURSOR shibboleth_auth_cur
        IS        
           SELECT t_gzbalma_pidm,
                  gobtpac_external_user
             FROM LOCAL.gzbalma, gobtpac c
            WHERE c.gobtpac_pidm = t_gzbalma_pidm
              AND c.gobtpac_external_user is not null
              AND c.gobtpac_activity_date <= (select max(gobtpac_activity_date)
                                                from gobtpac m
                                               where m.gobtpac_pidm = c.gobtpac_pidm
                                                 and m.gobtpac_activity_date <= SYSDATE);                                                                


        /*-------------------------------------------------------*/
        /* check to see if email address is too long to write    */
        /* to .xml file.                                         */
        /*-------------------------------------------------------*/
        CURSOR email_length_GT_90_char_cur
        IS
           SELECT t_gzbalma_pidm,
                  t_goremal_email,
                  LENGTH(t_goremal_email)
             FROM LOCAL.gzbalma
            WHERE LENGTH(t_goremal_email) > '90';


        /*-------------------------------------------------------*/
        /* if no oversize email address recs were found to skip, */
        /* write a default message line to .lis2 output file.    */
        /*-------------------------------------------------------*/
        CURSOR default_lis2_msg_cur
        IS
           SELECT COUNT(t_gzbalma_pidm) d_amt
             FROM LOCAL.gzbalma
            WHERE t_email_length_GT_90_char IS NOT NULL;            
                                                                                     
      
        /*-------------------------------------------------------*/
        /* read table with combined and distinct student and hr  */
        /* employee records and -- along with html markup below  */
        /* -- insert records into load table below.  said table  */
        /* is spooled and ourput placed in the jobsub directory  */
        /* as csmalma.xml.                                       */                                         
        /*-------------------------------------------------------*/                                    
        CURSOR load_with_xml_cur
        IS
           SELECT t_gzbalma_cwid,
                  t_spriden_first_name,
                  t_spriden_mi,
                  t_spriden_last_name,
                  t_goremal_email,
                  t_spraddr_phone,
                  t_gzbalma_blasterid,
                  t_gzbalma_user_group_desc,
                  t_gzbalma_user_group,
                  t_gobtpac_external_user,
                  t_calc_expiry_date
             FROM LOCAL.gzbalma
            WHERE t_email_length_GT_90_char IS NULL
         ORDER BY t_spriden_last_name, t_spriden_first_name, t_gzbalma_cwid; 
       

        /*------------------------------------------------------*/
        /* finally, check to see if any records were loaded in- */
        /* to the temp table which subsequent, non-banner pro-  */
        /* cessing is depending on. if not, raise an exception  */
        /* and end the program.                                 */
        /*------------------------------------------------------*/
        CURSOR empty_table_cur
        IS
           SELECT COUNT(load_last_name) d_load_count
             FROM LOCAL.gzbalma_load; 
                      

        /*-------------------------------------------------------*/
        /* define record datatypes for above cursor(s) so that   */
        /* a boatload of variables to not have to be coded and   */
        /* maintained.                                           */
        /*-------------------------------------------------------*/
        curr_vers_rec curr_vers_cur%ROWTYPE;
        get_hr_patrons_rec get_hr_patrons_cur%ROWTYPE;
        get_stu_patrons_rec get_stu_patrons_cur%ROWTYPE;
        shibboleth_auth_rec shibboleth_auth_cur%ROWTYPE;
				email_length_GT_90_char_rec email_length_GT_90_char_cur%ROWTYPE;
				default_lis2_msg_rec default_lis2_msg_cur%ROWTYPE;
        load_with_xml_rec load_with_xml_cur%ROWTYPE;
        empty_table_rec empty_table_cur%ROWTYPE;


        /*-------------------------------------------------------*/
        /* Programmer-defined exceptions. Processing is aborted.    */
        /*-------------------------------------------------------*/
        invalid_version EXCEPTION;
        no_hr_patrons EXCEPTION;
        no_stu_patrons EXCEPTION;
        no_records_found EXCEPTION;


BEGIN

----------------------------------
-- GET PARM VALUES FROM GJBPRUN --
----------------------------------
:bOutputFile := 'SPOOL ' ||OutputFile;
:bOutputFile2 := 'SPOOL ' ||OutputFile2;
:bOutputFile3 := 'SPOOL ' ||OutputFile3;
p_auto_sched  := GZKPARM.f_csm_get_parms(JobName, OneUpNo, '01');
SELECT TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') INTO v_today FROM DUAL;
SELECT SUBSTR(USER,1,15) INTO v_user FROM DUAL;
DBMS_OUTPUT.PUT_LINE('Date time started = '||v_today);
DBMS_OUTPUT.PUT_LINE('Job Name = '||JobName);
DBMS_OUTPUT.PUT_LINE('One Up No = '||OneUpNo);
DBMS_OUTPUT.PUT_LINE('User = '||v_user);
DBMS_OUTPUT.PUT_LINE('Version = '||v_curr_vers);
DBMS_OUTPUT.PUT_LINE('Incoming schedule type from form = '||p_auto_sched);

-------------------------------------
-- DELETE PARM VALUES FROM GJBPRUN --
-------------------------------------
GZKPARM.p_csm_del_parms(JobName, OneUpNo);

-------------------------------------------------------------------------
------------------------------ RUN THE PROGRAM --------------------------
-------------------------------------------------------------------------

        /*------------------------- STEP 0 ------------------------*/
        /* check to see if the version of the program being run is */
        /* the same as what Banner security expects it to be. if   */
        /* not, write msg to all output files and end the program. */
        /*---------------------------------------------------------*/
        IF NOT curr_vers_cur%ISOPEN
        THEN
        OPEN curr_vers_cur;
        END IF;
           FETCH curr_vers_cur INTO curr_vers_rec;

              IF v_curr_vers <> curr_vers_rec.guraobj_current_version
              THEN
              RAISE invalid_version;
              END IF;

        CLOSE curr_vers_cur;


        /*------------------------- STEP 1 ----------------------*/
        /* read hr patron records from hr temp table and load    */
        /* them into LOCAL.gzbalma for an eventual combined up-  */
        /* load to ALMA below, as long as a blasterid exists.    */
        /*-------------------------------------------------------*/
        IF NOT get_hr_patrons_cur%ISOPEN
        THEN
        OPEN get_hr_patrons_cur;
        END IF;
           LOOP
           FETCH get_hr_patrons_cur INTO get_hr_patrons_rec;

              IF get_hr_patrons_rec.t_pzpalma_id IS NULL
              THEN
              RAISE no_hr_patrons;
              END IF;

           EXIT WHEN get_hr_patrons_cur%NOTFOUND;
        
           INSERT INTO LOCAL.gzbalma VALUES (get_hr_patrons_rec.t_pzpalma_pidm,
                                             get_hr_patrons_rec.t_pzpalma_id,
                                             get_hr_patrons_rec.t_pzpalma_last_name,
                                             get_hr_patrons_rec.t_pzpalma_first_name,
                                             get_hr_patrons_rec.a_mi,
                                             get_hr_patrons_rec.t_pzpalma_bus_email,
                                             get_hr_patrons_rec.t_pzpalma_bus_phone,
                                             NULL,
                                             NULL,
                                             get_hr_patrons_rec.t_pzpalma_blasterid,
                                             get_hr_patrons_rec.a_user_group_desc,
                                             get_hr_patrons_rec.t_pzpalma_user_group,
                                             NULL,
                                             get_hr_patrons_rec.t_calc_expiry_date,
                                             get_hr_patrons_rec.DATA_SOURCE,
                                             v_user,
                                             v_today,
                                             v_curr_vers,
                                             NULL);

           END LOOP;        
        CLOSE get_hr_patrons_cur;       
        

        /*------------------------- STEP 2 ----------------------*/
        /* read student patron records from hr temp table and    */
        /* load them into LOCAL.gzbalma table for an eventual,   */
        /* combined upload to ALMA below, as long as long as     */
        /* the same CWID was not already loaded into LOCAL.      */
        /* gzbalma_from LOCAL.pzbalma.                           */
        /*-------------------------------------------------------*/
        IF NOT get_stu_patrons_cur%ISOPEN
        THEN
        OPEN get_stu_patrons_cur;
        END IF;
           LOOP
           FETCH get_stu_patrons_cur INTO get_stu_patrons_rec;

              IF get_stu_patrons_rec.szbalma_id IS NULL
              THEN
              RAISE no_stu_patrons;
              END IF;
              
           EXIT WHEN get_stu_patrons_cur%NOTFOUND;

           INSERT INTO LOCAL.gzbalma VALUES (get_stu_patrons_rec.szbalma_pidm,
                                             get_stu_patrons_rec.szbalma_id,
                                             get_stu_patrons_rec.szbalma_last_name,
                                             get_stu_patrons_rec.szbalma_first_name,
                                             get_stu_patrons_rec.b_mi,
                                             get_stu_patrons_rec.szbalma_email,
                                             get_stu_patrons_rec.b_fone,
                                             get_stu_patrons_rec.szbalma_levl_code,
                                             get_stu_patrons_rec.szbalma_dept_code,
                                             get_stu_patrons_rec.szbalma_blasterid,
                                             get_stu_patrons_rec.b_user_group_desc,
                                             get_stu_patrons_rec.szbalma_user_group_code,
                                             NULL,
                                             get_stu_patrons_rec.szbalma_expiry_date,
                                             NULL,
                                             v_user,
                                             v_today,
                                             v_curr_vers,
                                             NULL);
                                               
           END LOOP;
        CLOSE get_stu_patrons_cur;
        

        /*------------------------- STEP 3 ----------------------*/
        /* add shibboleth info to .xml output file. info appears */
        /* in UID user_identifier section.                       */ 
        /*-------------------------------------------------------*/
        IF NOT shibboleth_auth_cur%ISOPEN
        THEN
        OPEN shibboleth_auth_cur;
        END IF;
           LOOP
           FETCH shibboleth_auth_cur INTO shibboleth_auth_rec;
           EXIT WHEN shibboleth_auth_cur%NOTFOUND;

           UPDATE LOCAL.gzbalma
           SET t_gobtpac_external_user = shibboleth_auth_rec.gobtpac_external_user
           WHERE t_gzbalma_pidm = shibboleth_auth_rec.t_gzbalma_pidm;          
           
           END LOOP;
        CLOSE shibboleth_auth_cur;


        /*------------------------- STEP 4 ----------------------*/
        /* check to see if email address is too long to write    */
        /* to .xml file.                                         */
        /*-------------------------------------------------------*/
        IF NOT email_length_GT_90_char_cur%ISOPEN
        THEN
        OPEN email_length_GT_90_char_cur;
        END IF;
           LOOP
           FETCH email_length_GT_90_char_cur INTO email_length_GT_90_char_rec;
           EXIT WHEN email_length_GT_90_char_cur%NOTFOUND;

           UPDATE LOCAL.gzbalma
           SET t_email_length_GT_90_char = 'Y'
           WHERE t_gzbalma_pidm = email_length_GT_90_char_rec.t_gzbalma_pidm
           AND t_goremal_email = email_length_GT_90_char_rec.t_goremal_email;       
           
           END LOOP;
        CLOSE email_length_GT_90_char_cur;


        /*------------------------- STEP 5 ----------------------*/
        /* if no oversize email address recs were found to skip, */
        /* write a default message line to .lis2 output file.    */
        /*-------------------------------------------------------*/
        IF NOT default_lis2_msg_cur%ISOPEN
        THEN
        OPEN default_lis2_msg_cur;
        END IF;
           FETCH default_lis2_msg_cur INTO default_lis2_msg_rec;

              IF default_lis2_msg_rec.d_amt < 1
              THEN
              :bflag := 'N';
              :bdefault_msg := 'No long e-mail address records were found to skip.';
              ELSE
              :bflag := 'Y';
              END IF;

        CLOSE default_lis2_msg_cur;  
                              

        /*------------------------- STEP 6 ----------------------*/
        /* read table with combined and distinct student and hr  */
        /* employee records and -- along with html markup below  */
        /* -- insert records into load table below.  said table  */
        /* is spooled and ourput placed in the jobsub directory  */
        /* as csmalma.xml.                                       */  
        /*-------------------------------------------------------*/
        IF NOT load_with_xml_cur%ISOPEN
        THEN
        OPEN load_with_xml_cur;
        END IF;
           LOOP
           FETCH load_with_xml_cur INTO load_with_xml_rec;
           EXIT WHEN load_with_xml_cur%NOTFOUND;

           /*------------------------------------------------------------------------------------------------------*/
           /* these detail lines appear once for each fac/staff member in the mines-to-alma daily staff load file  */
           /*------------------------------------------------------------------------------------------------------*/
           INSERT INTO LOCAL.gzbalma_load VALUES ('  <user>',
                                                  '    <primary_id>'||load_with_xml_rec.t_gzbalma_cwid||'</primary_id>',                                              
                                                  '    <first_name>'||TRIM(load_with_xml_rec.t_spriden_first_name)||'</first_name>',
                                                  '    <middle_name>'||TRIM(load_with_xml_rec.t_spriden_mi)||'</middle_name>',
                                                  '    <last_name>'||TRIM(load_with_xml_rec.t_spriden_last_name)||'</last_name>',
                                                  '    <preferred_language desc="'||default_pref_language_desc||'">'||default_pref_language||'</preferred_language>',
                                                  '    <expiry_date>'||TO_CHAR(load_with_xml_rec.t_calc_expiry_date, 'YYYY-MM-DD')||'Z</expiry_date>',
                                                  '    <status desc="'||default_user_status_desc||'">'||default_user_status||'</status>',
                                                  '    <account_type desc="'||default_user_account_type_desc||'">'||default_user_account_type||'</account_type>',
                                                  '    <user_group desc="'||TRIM(load_with_xml_rec.t_gzbalma_user_group_desc)||'">'||TRIM(load_with_xml_rec.t_gzbalma_user_group)||'</user_group>',                                                                                                                         
                                                  '    <contact_info>',
                                                  '      <addresses>',
                                                  '        <address preferred="true">',
                                                  '          <line1>'||default_line1||'</line1>',
                                                  '          <city>'||default_city||'</city>',
                                                  '          <state_province>'||default_state_province||'</state_province>', 
                                                  '          <postal_code>'||default_postal_code||'</postal_code>',
                                                  '          <country>'||default_country||'</country>',                                                 
                                                  '          <start_date>'||TO_CHAR(SYSDATE, 'YYYY-MM-DD')||'Z</start_date>', 
                                                  '          <address_types>',                                                
                                                  '            <address_type desc="'||default_address_type_desc||'">'||default_address_type||'</address_type>',
                                                  '          </address_types>',                                                 
                                                  '        </address>',                                                 
                                                  '      </addresses>',
                                                  '      <emails>',
                                                  '        <email preferred="true">',
                                                  '          <email_address>',
                                                  TRIM(load_with_xml_rec.t_goremal_email),
                                                  '          </email_address>',
                                                  '          <email_types>',
                                                  '            <email_type desc="'||default_email_type_desc||'">'||default_email_type||'</email_type>',
                                                  '          </email_types>',                                                 
                                                  '        </email>',
                                                  '      </emails>',                                                                                        
                                                  '      <phones>',
                                                  '        <phone preferred="true" preferred_sms="false" segment_type="External">',
                                                  '          <phone_number>('||TRIM(SUBSTR(load_with_xml_rec.t_spraddr_phone,1,3)||') '||
                                                                                    SUBSTR(load_with_xml_rec.t_spraddr_phone,4,3)||'-'||
                                                                                    SUBSTR(load_with_xml_rec.t_spraddr_phone,7,4))||'</phone_number>',
                                                  '          <phone_types>',
                                                  '            <phone_type desc="'||default_phone_type_desc||'">'||default_phone_type||'</phone_type>',                                            
                                                  '          </phone_types>',
                                                  '      </phone>',                                                  
                                                  '      </phones>',                                                                                                   
                                                  '    </contact_info>',
                                                  '    <user_identifiers>',
                                                  '      <user_identifier segment_type="External">',
                                                  '        <id_type desc="Barcode">BARCODE</id_type>',
                                                  '        <value>'||load_with_xml_rec.t_gzbalma_blasterid||'</value>',
                                                  '        <note>'||TO_CHAR(SYSDATE, 'DD-MON-YYYY')||'</note>',
                                                  '        <status>ACTIVE</status>',
                                                  '      </user_identifier>',
                                                  '      <user_identifier segment_type="External">',
                                                  '        <id_type desc="UID">UID</id_type>',
                                                  '        <value>'||TRIM(load_with_xml_rec.t_gobtpac_external_user)||'</value>',
                                                  '        <note>'||TO_CHAR(SYSDATE, 'DD-MON-YYYY')||'</note>',
                                                  '        <status>ACTIVE</status>',
                                                  '      </user_identifier>',
                                                  '    </user_identifiers>',                                               
                                                  '  </user>');
                                              
           END LOOP;        
        CLOSE load_with_xml_cur;


        /*------------------------- STEP 7 ----------------------*/
        /* finally, check to see if any records were loaded into */
        /* the temp table which subsequent, non-banner process-  */
        /* ing is depending on. if not, raise an exception and   */
        /* end the program.                                      */
        /*-------------------------------------------------------*/
        IF NOT empty_table_cur%ISOPEN
        THEN
        OPEN empty_table_cur;
        END IF;
           LOOP
           FETCH empty_table_cur INTO empty_table_rec;

           IF empty_table_rec.d_load_count < 1
           THEN
           RAISE no_records_found;
           END IF;

           EXIT WHEN empty_table_cur%NOTFOUND;

           END LOOP;
        CLOSE empty_table_cur;


        /*------------------------- STEP 8 ----------------------*/
        /* send e-mail to listed people to verify a successful   */
        /* directory load.                                       */
        /*-------------------------------------------------------*/
        SELECT 'Number of records in LOCAL.gzbalma_load ' || count(*)
        INTO v_message_text
        FROM LOCAL.gzbalma_load;
        
        IF p_auto_sched = 'N'
        THEN

        gzpnoti('Library.Feed@mines.edu',
                'panderse@mines.edu',
                'Alma Joint Libr Patron Load ' || SYSDATE,
                 v_message_text);

        gzpnoti('Library.Feed@mines.edu',
                'lguy@mines.edu',
                'Alma Joint Libr Patron Load ' || SYSDATE,
                 v_message_text);

        gzpnoti('Library.Feed@mines.edu',
                'tramstet@mines.edu',
                'Alma Joint Libr Patron Load ' || SYSDATE,
                 v_message_text);

        gzpnoti('Library.Feed@mines.edu',
                'lcramer@mines.edu',
                'Alma Joint Libr Patron Load ' || SYSDATE,
                 v_message_text);

        gzpnoti('Library.Feed@mines.edu',
                'pscofiel@mines.edu',
                'Alma Joint Libr Patron Load ' || SYSDATE,
                 v_message_text);

        END IF;


        /*------------------------- STEP 9 ----------------------*/
        /* write eoj PL/SQL messages to the .log and .lis files. */
        /*-------------------------------------------------------*/
        DBMS_OUTPUT.PUT_LINE('-----');
        DBMS_OUTPUT.PUT_LINE('Running ''Alma Joint Libr Patron Load'' Program');
        IF :bflag = 'Y'
        THEN
        DBMS_OUTPUT.PUT_LINE('** Skipped records exist in .lis2 report. Contact ES. **');
        END IF;
        DBMS_OUTPUT.PUT_LINE('Reached good EOJ for PL/SQL');
        DBMS_OUTPUT.PUT_LINE('-----');


EXCEPTION

        /*------------------------ STEP 10 ----------------------*/
        /* Error handling if exception raised in any of the fol- */
        /* lowing STEP(S): 0-2 and 7.                            */
        /*-------------------------------------------------------*/
        WHEN invalid_version
        THEN
        DBMS_OUTPUT.PUT_LINE ('-----');
        :berror_msg := NULL;
        :berror_msg := '** Version number mismatch with Banner security. Job ended. **';
        DBMS_OUTPUT.PUT_LINE (:berror_msg);
        DBMS_OUTPUT.PUT_LINE ('-----');
        ROLLBACK;

        WHEN no_hr_patrons
        THEN
        DBMS_OUTPUT.PUT_LINE ('-----');
        :berror_msg := NULL;
        :berror_msg := '** No HR Patron Load records found to process. Job ended. **';
        DBMS_OUTPUT.PUT_LINE (:berror_msg);
        DBMS_OUTPUT.PUT_LINE ('-----');
        ROLLBACK;

        WHEN no_stu_patrons
        THEN
        DBMS_OUTPUT.PUT_LINE ('-----');
        :berror_msg := NULL;
        :berror_msg := '** No Student Patron Load records found to process. Job ended. **';
        DBMS_OUTPUT.PUT_LINE (:berror_msg);
        DBMS_OUTPUT.PUT_LINE ('-----');
        ROLLBACK;
                
        WHEN no_records_found
        THEN
        DBMS_OUTPUT.PUT_LINE ('-----');
        :berror_msg := NULL;
        :berror_msg := '** No Alma Joint Libr Patron Load records found to process. Job ended. **';
        DBMS_OUTPUT.PUT_LINE (:berror_msg);
        DBMS_OUTPUT.PUT_LINE ('-----');
        ROLLBACK;

COMMIT;
NULL;
END;
/
/*--------------------------*/
/* END MAINLINE PROCESSING  */
/*--------------------------*/
SET TERMOUT OFF;
SET VERIFY OFF;
SET HEADING OFF;
SET ECHO OFF;
SET PAGESIZE 0;
SET LINESIZE 132;
SET SPACE 0;
TTITLE OFF;
BTITLE OFF;
/*---------------------------*/
/* create default .lis file  */
/*---------------------------*/
SPOOL gzpalma.temp;
EXEC DBMS_OUTPUT.PUT_LINE(:bOutputFile);
SPOOL OFF;
START gzpalma.temp
SELECT 'Data contained in LOCAL.gzbalma, LOCAL.gzbalma_load and related csmpatronload.xml file.' FROM DUAL
WHERE :berror_msg IS NULL;
--
SELECT 'Message:  '||:berror_msg
FROM DUAL
WHERE :berror_msg IS NOT NULL;
SET HEADING ON;
SPOOL OFF;
/*-----------------------------------------------------*/
/* create the csmpatronload.xml file and write it to   */
/* the /banjobs_<instance_name> directory.             */
/*-----------------------------------------------------*/
TTITLE OFF;
BTITLE OFF;
SET HEADING OFF;
SET PAGESIZE 0;
SET LINESIZE 90;
SET ECHO OFF;
SET TIMING OFF;
SET FEEDBACK OFF;
SET VERIFY OFF;
SET DOC OFF;
SET TERMOUT OFF;
SET RECSEP OFF;
SPOOL gzpalma.temp2;
EXEC DBMS_OUTPUT.PUT_LINE(:bOutputFile2);
SPOOL OFF;
START gzpalma.temp2
--------------------------------
-- this writes one header row --
---------------------------------
SELECT '<users>' FROM DUAL 
WHERE :berror_msg IS NULL;
----------------------------------
-- this selects all detail rows --
----------------------------------
SELECT *
FROM LOCAL.gzbalma_load
WHERE :berror_msg IS NULL;
--------------------------------
-- this writes one footer row --
--------------------------------
SELECT '</users>' FROM DUAL
WHERE :berror_msg IS NULL;
--
SELECT 'Message:  '||:berror_msg
FROM DUAL
WHERE :berror_msg IS NOT NULL;
SET HEADING ON;
SPOOL OFF;
/*-------------------------------------------------*/
/* write skipped records (those having an excess-  */
/* ively long goremail_email value) to the .lis2   */
/* file.                                           */
/*-------------------------------------------------*/
SET TERMOUT OFF;
SET VERIFY OFF;
SET ECHO OFF;
SET PAGESIZE 45;
SET SPACE 2;
SET LINESIZE 117;
SPOOL gzpalma.temp3;
EXEC DBMS_OUTPUT.PUT_LINE(:bOutputFile3);
SPOOL OFF;
START gzpalma.temp3
COLUMN rptdate new_value xdate noprint
COLUMN rpttime new_value xtime noprint
SELECT TO_CHAR(SYSDATE, 'DD-MON-YYYY') "rptdate",
       TO_CHAR(SYSDATE, 'HH24:MI:SS') "rpttime"
FROM DUAL;
TTITLE ON;
TTITLE LEFT 'GZPAMLA' CENTER  'Colorado School of Mines' -
col 103 xdate skip 1 -
LEFT xtime -
CENTER 'Alma Joint Libr Patron Load - Skipped Records (GOREMAL_EMAIL Length)' -
col 103 'Page: '  FORMAT 9999 SQL.PNO skip 2
COLUMN t_gzbalma_cwid FORMAT A8 HEADING 'CWID'; 
COLUMN Name FORMAT A40 HEADING 'Patron Name';
COLUMN Email FORMAT A50 Heading 'E-mail Address'
COLUMN t_gzbalma_pidm FORMAT 99999999 HEADING 'Pidm' JUSTIFY RIGHT; 
COLUMN :bdefault_msg FORMAT A50 HEADING 'Message'
SET NEWPAGE 0;
SELECT t_gzbalma_cwid,
       SUBSTR(TRIM(t_spriden_last_name)||', '||TRIM(t_spriden_first_name)||' '||TRIM(t_spriden_mi),1,40) "Name",
       SUBSTR(t_goremal_email,1,50) "Email",
       t_gzbalma_pidm
FROM LOCAL.gzbalma
WHERE t_email_length_GT_90_char IS NOT NULL
AND :berror_msg IS NULL
ORDER BY SUBSTR(TRIM(t_spriden_last_name)||', '||TRIM(t_spriden_first_name)||' '||TRIM(t_spriden_mi),1,40), t_gzbalma_cwid;
--
-- write default message to .lis2 file if no skipped records were found
--
SELECT :bdefault_msg
FROM DUAL
WHERE :bflag = 'N'
AND :berror_msg IS NULL;
--
-- write regular (exception-related) error messages to all files
--
SET HEADING OFF;
SELECT 'Message:  '||:berror_msg
FROM DUAL
WHERE :berror_msg IS NOT NULL;
SET HEADING ON;
SPOOL OFF;
/*----------------------------------*/
/* END FORMATTING. DO HOUSEKEEPING  */
/*----------------------------------*/
COMMIT;
EXIT SQL.SQLCODE;
EXIT